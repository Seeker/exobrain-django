# Django for my Zettelkasten

## Goals

- Produce a way to serve my Zettelkasten as a website

## Installation

Clone the repo and run

```sh
poetry install
```
