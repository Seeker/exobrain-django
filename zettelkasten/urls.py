from django.urls import path

from zettelkasten import views

urlpatterns = [
        path("<slug:slug>", views.note),
]
